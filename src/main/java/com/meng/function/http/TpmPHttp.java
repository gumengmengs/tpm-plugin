package com.meng.function.http;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.meng.util.Dict;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import java.util.List;


public class TpmPHttp extends HttpDispose {

    private Player player;

    private Integer state;

    private Integer second;

    private JsonObject jsonObject;

    public TpmPHttp(JsonObject jsonObject, String ip, String url, Player player, Integer state) {
        super(jsonObject, ip, url);
        this.player = player;
        this.state = state;
        second = Dict.TPM_P_SECOND;
    }

    @Override
    public void run() {
        super.msgHttpPost();
        switch (state) {
            case 101:
                tpmP();
                break;
            case 102:
                msgUser();
                break;
            case 103:
                TpmGet();
                break;
        }
    }

    public void startTpmP() {
        runTaskTimer(Dict.PLUGIN, 0, 30L);
    }

    public void stop() {
        this.cancel();
    }

    private void msgUser() {
        String result = String.valueOf(super.getResult());
        player.sendMessage(result);
    }

    private void tpmP() {
        try {
            if (second == Dict.TPM_P_SECOND)
                jsonObject = JsonParser.parseString(String.valueOf(super.getResult())).getAsJsonObject();
            if (jsonObject.get("msg") != null) {
                player.sendMessage(jsonObject.get("msg").getAsString());
                stop();
                return;
            }
            String addressName = jsonObject.get("addressName").getAsString();
            if (second != 0) {
                player.sendMessage(second + "秒后传送到:" + addressName);
                second--;
                return;
            }
            String worldName = jsonObject.get("worldName").getAsString();

            List<World> worlds = player.getServer().getWorlds();
            World world = null;
            for (World worldT : worlds) {
                if (worldT.getName().equals(worldName)) {
                    world = worldT;
                }
            }
            player.teleport(new Location(world,
                    jsonObject.get("x").getAsDouble(),
                    jsonObject.get("y").getAsDouble(),
                    jsonObject.get("z").getAsDouble()));
            player.sendMessage("已传送到: " + addressName);
            stop();
        } catch (Exception e) {
            e.printStackTrace();
            player.sendMessage("发生异常,请联系服务器负责人");
            stop();
        }
    }

    private void TpmGet() {
        jsonObject = JsonParser.parseString(String.valueOf(super.getResult())).getAsJsonObject();
        if (jsonObject.get("state").getAsInt() == 100) {
            JsonArray jsonArray = jsonObject.get("arr").getAsJsonArray();
            for (JsonElement jsonElement : jsonArray) {
                JsonObject asJsonObject = jsonElement.getAsJsonObject();
                player.sendMessage(
                        "在<" + asJsonObject.get("worldName").getAsString() + ">世界里的地址: " +
                                asJsonObject.get("addressName").getAsString()
                );
            }
        }else{
            player.sendMessage(jsonObject.get("msg").getAsString());
        }
    }
}
