package com.meng.function.tpm.util;

import com.meng.util.Dict;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class TpmRunnable extends BukkitRunnable {
    private Integer second;

    private CommandSender sender;

    private Player toPlayer;


    public TpmRunnable(CommandSender sender, Player toPlayer) {
        this.sender = sender;
        this.toPlayer = toPlayer;
        //读取配置 选定是多少秒
        this.second = Dict.TPM_SECOND;
    }

    @Override
    public void run() {
        if (second != 0){
            sender.sendMessage(second + "秒后传送至玩家 " + toPlayer.getName());
            toPlayer.sendMessage("玩家 " + sender.getName() + " 将在" + second + "秒后传送到您的身边");
            if (second == Dict.TPM_SECOND) {
                toPlayer.sendMessage("如果拒绝输入\"\\tpm refuse [player]\"");
                sender.sendMessage("如果取消传送输入\"\\tpm cancel\"");
            }
            second--;
        }else{
            TpmCommon.isOnline(sender,sender.getName()).teleport(new Location(toPlayer.getWorld(),
                    toPlayer.getLocation().getX(),
                    toPlayer.getLocation().getY(),
                    toPlayer.getLocation().getZ()));
            toPlayer.sendMessage("玩家" + sender.getName() + "传送到了你身边");
            stop(sender.getName());
        }
    }

    public void start(){
        this.runTaskTimer(Dict.PLUGIN,0,40L);
    }

    public void stop(String player){
        this.cancel();
        TpmCommon.playerMap.remove(player);
    }

    public void cancel(String player){
        stop(player);
        toPlayer.sendMessage("玩家 " + player + " 取消了传送到您身边");
    }
}
