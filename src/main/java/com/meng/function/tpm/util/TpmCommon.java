package com.meng.function.tpm.util;

import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.HashMap;

/**
 * @author GuMeng
 */
public class TpmCommon {
    /**
     * 前面的String 是 tp 者
     */
    public static HashMap<String, TpmRunnable> playerMap = new HashMap<>();

    /**
     * 返回后者的player属性
     */
    public static Player isOnline(CommandSender sender, String arg) {
        Server server = sender.getServer();
        Collection<? extends Player> onlinePlayers = server.getOnlinePlayers();
        Player toPlayer = null;
        for (Player onlinePlayer : onlinePlayers) {
            if (onlinePlayer.getName().equals(arg)) {
                toPlayer = onlinePlayer;
            }
        }
        return toPlayer;
    }
}

