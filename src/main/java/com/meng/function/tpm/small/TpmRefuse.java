package com.meng.function.tpm.small;

import com.meng.function.tpm.util.TpmCommon;
import com.meng.function.tpm.util.TpmRunnable;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class TpmRefuse {
    public boolean onCommand(@NotNull CommandSender sender, @NotNull String arg) {
        Player player = TpmCommon.isOnline(sender, arg);
        if (player != null) {
            TpmRunnable tpmRunnable = TpmCommon.playerMap.get(player.getName());
            if (tpmRunnable != null) {
                tpmRunnable.stop(player.getName());
                player.sendMessage(sender.getName() + "拒绝您传送到他的身边");
            } else sender.sendMessage("该玩家当前没有向你传送");
        }
        return true;
    }
}
