package com.meng.function.tpm.small;

import com.meng.function.tpm.util.TpmRunnable;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import static com.meng.function.tpm.util.TpmCommon.playerMap;

public class TpmCancel{
    public boolean onCommand(@NotNull CommandSender sender) {
        TpmRunnable tpmThread = playerMap.get(sender.getName());
        if (tpmThread != null) {
            tpmThread.cancel(sender.getName());
            sender.sendMessage("已取消传送");
        } else {
            sender.sendMessage("当前没有传送目标");
        }
        return true;
    }
}
