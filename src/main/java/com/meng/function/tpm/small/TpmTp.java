package com.meng.function.tpm.small;

import com.meng.function.tpm.util.TpmRunnable;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import static com.meng.function.tpm.util.TpmCommon.isOnline;
import static com.meng.function.tpm.util.TpmCommon.playerMap;

public class TpmTp{
    public boolean onCommand(@NotNull CommandSender sender, @NotNull String arg) {
        boolean result = false;
        if (isOnline(sender, arg) != null) {
            if (playerMap.get(sender.getName()) == null) {
                TpmRunnable tpmRunnable = new TpmRunnable(sender, isOnline(sender, arg));
                playerMap.put(sender.getName(), tpmRunnable);
                tpmRunnable.start();
            } else sender.sendMessage("当前有传送目标,请先取消传送");
            result = true;
        }
        return result;
    }

    public boolean onCommand(@NotNull Player player, @NotNull String arg) {
        boolean result = false;
        if (isOnline(player, arg) != null) {
            if (playerMap.get(player.getName()) == null) {
                TpmRunnable tpmRunnable = new TpmRunnable(player, isOnline(player, arg));
                playerMap.put(player.getName(), tpmRunnable);
                tpmRunnable.start();
            } else player.sendMessage("当前有传送目标,请先取消传送");
            result = true;
        }
        return result;
    }
}
