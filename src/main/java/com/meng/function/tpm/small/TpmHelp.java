package com.meng.function.tpm.small;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.TextComponent;
import net.kyori.adventure.text.event.ClickEvent;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

public class TpmHelp{
    public boolean onCommand(@NotNull CommandSender sender) {
        HashMap<String,String> commandMap = new HashMap<>(16);
        commandMap.put("/tpm-cd -----> 打开tpm的菜单","/tpm-cd");
        commandMap.put("/tpm [玩家] -----> 传送到某个玩家身边","/tpm");
        commandMap.put("/tpm refuse [玩家] -----> 传送到某个玩家身边","/tpm refuse");
        commandMap.put("/tpm cancel -----> 取消传送","/tpm cancel");
        commandMap.put("/tpm help -----> 获取tpm帮助","/tpm help");
        commandMap.put("/tpm-p add [地址名] -----> 添加地址","/tpm-p add");
        commandMap.put("/tpm-p [地址名] -----> 传送地址","/tpm-p");
        commandMap.put("/tpm-p del [地址名] -----> 删除地址","/tpm-p del");
        commandMap.put("/tpm-p get -----> 获取地址","/tpm-p get");
        commandMap.put("/tpm-p cancel -----> 取消传送","/tpm-p cancel");
        commandMap.put("/tpm-p cancel-cancel -----> 取消传送","/tpm-p cancel-cancel");
        commandMap.put("/tpm-p share [地址名] [玩家名(如果为空则默认分享给全部玩家)] -----> 分享地址","/tpm-p share");
        sender.sendMessage("§6###################tpm help####################");
        int i = 1;
        for (String key : commandMap.keySet()){
            TextComponent tpm = Component.text("[" + i++ + "]" + key)
                    .clickEvent(ClickEvent.suggestCommand(commandMap.get(key)));
            sender.sendMessage(tpm);
        }
        sender.sendMessage("§6###################点击免输指令###################");
        commandMap.clear();
        commandMap.clone();
        return true;
    }
}
