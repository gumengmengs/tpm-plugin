package com.meng.function.tpm;

import com.meng.function.tpm.small.TpmCancel;
import com.meng.function.tpm.small.TpmHelp;
import com.meng.function.tpm.small.TpmRefuse;
import com.meng.function.tpm.small.TpmTp;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

public class Tpm implements TabExecutor {
    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 0) return false;
        switch (args[0]) {
            case "cancel":
                return new TpmCancel().onCommand(sender);
            case "refuse":
                return new TpmRefuse().onCommand(sender,args[1]);
            case "help":
                return new TpmHelp().onCommand(sender);
            default:
                return new TpmTp().onCommand(sender,args[0]);
        }
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        List<String> list = new LinkedList<>();
        for (Player onlinePlayer : sender.getServer().getOnlinePlayers()) {
            list.add(onlinePlayer.getName());
        }
        if (args.length == 0 || args.length == 1) {
            list.add("cancel");
            list.add("refuse");
            list.add("help");
        } else if (args[0].equals("cancel") || args[0].equals("help")) {
            return null;
        }
        return list;
    }
}
