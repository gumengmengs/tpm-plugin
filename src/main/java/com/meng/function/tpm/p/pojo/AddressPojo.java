package com.meng.function.tpm.p.pojo;

public class AddressPojo {
    private String addressName;
    private String worldName;
    private Double x;
    private Double y;
    private Double z;

    private String isShare;

    public AddressPojo(){

    }

    public AddressPojo(String addressName, String worldName, Double x, Double y, Double z) {
        this.addressName = addressName;
        this.worldName = worldName;
        this.x = x;
        this.y = y;
        this.z = z;
        //默认分享状态为refuse(拒绝)
        this.isShare = "refuse";
    }

    public String getAddressName() {
        return addressName;
    }

    public String getWorldName() {
        return worldName;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public Double getZ() {
        return z;
    }

    public String getIsShare() {
        return isShare;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public void setZ(Double z) {
        this.z = z;
    }

    public void setIsShare(String isShare) {
        this.isShare = isShare;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }
}
