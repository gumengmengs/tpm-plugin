package com.meng.function.tpm.p.small;

import com.meng.file.Address;
import com.meng.function.gui.TpmGUI;
import com.meng.function.tpm.p.pojo.AddressPojo;
import com.meng.function.tpm.p.util.Local;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

public class TpmPCancelShare {
    public boolean onCommand(@NotNull CommandSender sender, String addressName) {
        List<HashMap<String, Object>> allAddress = new Local().getAllAddress(sender.getName());
        AddressPojo addressPojo = new AddressPojo();
        addressPojo.setAddressName(addressName);
        for (HashMap<String, Object> address : allAddress) {
            String isShare = (String) address.get("isShare");
            String addressName1 = (String) address.get("addressName");
            if (!isShare.equals("refuse")){
                if (addressName1.equals(addressName)){
                    addressPojo.setIsShare("refuse");
                    new Local().upData(addressPojo,sender.getName());
                    sender.sendMessage("已成功取消分享" + addressName + "权限");
                    if (TpmGUI.mapDeleteRunnableList.size() > 0 && TpmGUI.mapDeleteRunnableList.get(0).state != 0) TpmGUI.mapDeleteRunnableList.get(0).run();
                    return true;
                }
            }
        }
        return false;
    }
}
