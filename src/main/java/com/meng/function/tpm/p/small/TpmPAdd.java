package com.meng.function.tpm.p.small;

import com.google.gson.JsonObject;
import com.meng.function.http.TpmPHttp;
import com.meng.function.tpm.p.pojo.AddressPojo;
import com.meng.function.tpm.p.util.Local;
import com.meng.util.Dict;
import org.bukkit.entity.Player;

public class TpmPAdd{
    public boolean onCommand(Player player,String arg) {
        String worldName = player.getWorld().getName();
        double x = player.getLocation().getX();
        double y = player.getLocation().getY();
        double z = player.getLocation().getZ();

//        if (true){
            localAdd(arg,worldName,x,y,z,player);
            if (Dict.ADDRESS_SERVER) serverAdd(arg,worldName,x,y,z,player);
//        }else if (Dict.ADDRESS_SERVER) serverAdd(arg,worldName,x,y,z,player);
//        else localAdd(arg,worldName,x,y,z,player);
        return true;
    }

    public boolean onCommand(Player player,String arg,double x,double y,double z,String worldName){
        localAdd(arg,worldName,x,y,z,player);
        if (Dict.ADDRESS_SERVER) serverAdd(arg,worldName,x,y,z,player);
        return true;
    }

    private void localAdd(String arg,String worldName,double x,double y,double z,Player player){
        AddressPojo addressPojo = new AddressPojo(arg,worldName,x,y,z);
        if (new Local().addAddress(addressPojo,player.getName())) {
            player.sendMessage("添加成功");
        }else{
            player.sendMessage("失败，添加重复，请删除地址再经行添加");
        }
    }

    private void serverAdd(String arg,String worldName,double x,double y,double z,Player player){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userName",player.getName());
        jsonObject.addProperty("worldName",worldName);
        jsonObject.addProperty("x",x);
        jsonObject.addProperty("y",y);
        jsonObject.addProperty("z",z);
        jsonObject.addProperty("addressName",arg);
        new TpmPHttp(jsonObject,Dict.IP,"address/add",player,102).start();
    }
}
