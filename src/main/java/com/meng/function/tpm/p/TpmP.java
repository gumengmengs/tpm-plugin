package com.meng.function.tpm.p;

import com.meng.file.Address;
import com.meng.function.http.TpmPHttp;
import com.meng.function.tpm.p.small.*;
import com.meng.function.tpm.p.util.TpmPRunnable;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.meng.function.tpm.util.TpmCommon.isOnline;

public class TpmP implements TabExecutor {

    public static HashMap<String, TpmPHttp> addressTpUser = new HashMap<>();
    public static HashMap<String, TpmPRunnable> addressTpUserLocal = new HashMap<>();

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if (args.length == 0) {
            return false;
        }
        switch (args[0]) {
            case "add":
                if (args[1].equals("add") || args[1].equals("del") || args[1].equals("get")) {
                    sender.sendMessage("地址名不能为\"[add]、[del]、[get]\"");
                    return true;
                }
                return new TpmPAdd().onCommand(isOnline(sender, sender.getName()), args[1]);
            case "del":
                return new TpmPDel().onCommand(sender, args[1]);
            case "get":
                return new TpmPGet().onCommand(sender);
            case "share":
                if (args.length == 2) {
                    return new TpmPShare().onCommand(isOnline(sender, sender.getName()), args[1], null);
                }
                return new TpmPShare().onCommand(isOnline(sender, sender.getName()), args[1], isOnline(sender, args[2]));
            case "share-add":
                return new TpmPShareAdd().onCommand(isOnline(sender, sender.getName()), args[1], args[2]);
            case "cancel":
                return new TpmPCancel().onCommand(sender);
            case "share-cancel":
                return new TpmPCancelShare().onCommand(sender,args[1]);
            default:
                return new TpmPTp().onCommand(isOnline(sender, sender.getName()), args[0]);
        }
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        List<String> list = new LinkedList<>();
        list.add("<地址名>");
        if (args.length == 0 || args.length == 1) {
            list.add("add");
            list.add("del");
            list.add("get");
            list.add("share");
            list.add("cancel");
            list.add("share-cancel");
            List<Map<?, ?>> mapList = new Address().getAddress().getMapList("address." + sender.getName());
            for (Map<?, ?> map : mapList) {
                list.add((String) map.get("addressName"));
            }
        } else if (args[0].equals("get") || args[0].equals("cancel")) {
            return null;
        } else if (args[0].equals("del") || args[0].equals("share") || args[0].equals("share-cancel")) {
            if (args.length == 2) {
                List<Map<?, ?>> mapList = new Address().getAddress().getMapList("address." + sender.getName());
                for (Map<?, ?> map : mapList) {
                    list.add((String) map.get("addressName"));
                }
            }else if (args[0].equals("share") && args.length == 3){
                for (Player onlinePlayer : sender.getServer().getOnlinePlayers()) {
                    list.add(onlinePlayer.getName());
                }
            }
        }
        return list;
    }
}
