package com.meng.function.tpm.p.small;

import com.google.gson.JsonObject;
import com.meng.function.http.TpmPHttp;
import com.meng.function.tpm.p.util.Local;
import com.meng.function.tpm.p.util.TpmPRunnable;
import com.meng.util.Dict;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;

import static com.meng.function.tpm.p.TpmP.addressTpUser;
import static com.meng.function.tpm.p.TpmP.addressTpUserLocal;

public class TpmPTp {
    public boolean onCommand(@NotNull Player player, @NotNull String arg) {
        if (addressTpUserLocal.get(player.getName()) == null) {
            tpmP(player.getName(), arg, player);
        } else {
            player.sendMessage("当前正在传送,请取消后再经行传送");
        }
        return true;
    }

    private void tpmP(String userName, String addressName, Player player) {
        HashMap<String, Object> hashMap = new Local().tpAddress(userName, addressName);
        String msg = (String) hashMap.get("msg");
        if (msg != null && Dict.ADDRESS_SERVER) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("addressUserName", addressName);
            jsonObject.addProperty("userName", userName);
            TpmPHttp addressTp = new TpmPHttp(jsonObject, Dict.IP, "address/get", player, 101);
            addressTpUser.put(player.getName(), addressTp);
            addressTp.startTpmP();
        } else if (msg != null) player.sendMessage(msg);
        else {
            TpmPRunnable tpmPRunnable = new TpmPRunnable(player, hashMap);
            addressTpUserLocal.put(player.getName(), tpmPRunnable);
            tpmPRunnable.start();
        }
    }

    public void tpmPAddress(@NotNull Player player, @NotNull String arg, @NotNull String userName) {
        if (addressTpUserLocal.get(player.getName()) == null) {
            tpmP(userName, arg, player);
        } else {
            player.sendMessage("当前正在传送,请取消后再经行传送");
        }
    }
}
