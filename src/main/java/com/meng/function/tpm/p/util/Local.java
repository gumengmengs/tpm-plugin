package com.meng.function.tpm.p.util;

import com.google.gson.*;
import com.meng.file.Address;
import com.meng.function.tpm.p.pojo.AddressPojo;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Local {
    public boolean addAddress(AddressPojo addressPojo, String userName) {
        Address address = new Address();
        List<Map<?, ?>> mapList = address.getAddress().getMapList("address." + userName);
        JsonArray asJsonArray;
        JsonObject jsonObject = JsonParser.parseString(new GsonBuilder().setPrettyPrinting().create().toJson(addressPojo)).getAsJsonObject();
        List<HashMap<String, String>> outList = new LinkedList<>();
        if (mapList.size() == 0) {
            asJsonArray = new JsonArray();
            asJsonArray.add(jsonObject);
        } else {
            for (int i = 0; i < mapList.size(); i++) {
                Map<?, ?> map = mapList.get(i);
                outList.add(i, (HashMap<String, String>) map);
                if (map.get("addressName").toString().equals(addressPojo.getAddressName())) return false;
            }
        }
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap = new Gson().fromJson(jsonObject, hashMap.getClass());
        outList.add(hashMap);
        address.getAddress().set("address." + userName, outList);
        address.saveAddress();
        return true;
    }

    public HashMap<String, Object> tpAddress(String userName, String addressName) {
        Address address = new Address();
        List<Map<?, ?>> mapList = address.getAddress().getMapList("address." + userName);
        HashMap<String, Object> outMap = new HashMap<>();
        for (Map<?, ?> map : mapList) {
            if (map.get("addressName").toString().equals(addressName)) {
                outMap.putAll((Map<? extends String, ?>) map);
                return outMap;
            }
        }
        outMap.put("msg", "未找到传送地点，请确认全部传送地点");
        return outMap;
    }

    public List<HashMap<String, Object>> getAllAddress(String userName) {
        Address address = new Address();
        List<Map<?, ?>> mapList = address.getAddress().getMapList("address." + userName);
        List<HashMap<String, Object>> outList = new LinkedList<>();
        for (int i = 0; i < mapList.size(); i++) {
            Map<?, ?> map = mapList.get(i);
            outList.add(i, (HashMap<String, Object>) map);
        }
        return outList;
    }

    public String delAddress(String userName, String addressName) {
        try {
            Address address = new Address();
            List<Map<?, ?>> mapList = address.getAddress().getMapList("address." + userName);
            List<HashMap<String, String>> outList = new LinkedList<>();
            int j = mapList.size();
            for (Map<?, ?> map : mapList) {
                if (map.get("addressName").toString().equals(addressName)) {
                    j--;
                    continue;
                }
                outList.add((HashMap<String, String>) map);
            }
            if (j == mapList.size()) {
                return "没有找到传送点，请检查所有传送点后再删除";
            }
            address.getAddress().set("address." + userName, outList);
            address.saveAddress();
            return "删除成功";
        } catch (Exception e) {
            e.printStackTrace();
            return "发生未知错误,请联系服务器管理员";
        }
    }

    public String upData(AddressPojo addressPojo, String userName) {
        Address address = new Address();
        List<Map<?, ?>> mapList = address.getAddress().getMapList("address." + userName);
        List<HashMap<String, Object>> outList = new LinkedList<>();
        int j = mapList.size();
        for (int i = 0; i < mapList.size(); i++) {
            Map<?, ?> map = mapList.get(i);
            outList.add(i, (HashMap<String, Object>) map);
            if (map.get("addressName").equals(addressPojo.getAddressName())) {
                HashMap<String, Object> stringObjectHashMap = outList.get(i);
                if (addressPojo.getX() != null) stringObjectHashMap.put("x", addressPojo.getX());
                if (addressPojo.getY() != null) stringObjectHashMap.put("y", addressPojo.getX());
                if (addressPojo.getZ() != null) stringObjectHashMap.put("z", addressPojo.getX());
                if (addressPojo.getIsShare() != null){
                    stringObjectHashMap.put("isShare", addressPojo.getIsShare());
                }
                outList.set(i,stringObjectHashMap);
                j--;
            }
        }
        if (j == mapList.size()) {
            return "没有找到传送点，请检查所有传送点后再更改";
        }
        address.getAddress().set("address." + userName, outList);
        address.saveAddress();
        return "更改成功";
    }
}
