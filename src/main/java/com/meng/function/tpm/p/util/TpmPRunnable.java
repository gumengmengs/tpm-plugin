package com.meng.function.tpm.p.util;

import com.meng.function.gui.TpmGUI;
import com.meng.util.Dict;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.List;

import static com.meng.function.tpm.p.TpmP.addressTpUserLocal;

public class TpmPRunnable extends BukkitRunnable {
    private Integer second;

    private Player player;

    private HashMap<String, Object> hashMap;


    public TpmPRunnable(Player player, HashMap<String, Object> hashMap) {
        this.player = player;
        //读取配置 选定是多少秒
        second = Dict.TPM_P_SECOND;
        this.hashMap = hashMap;
    }

    @Override
    public void run() {
        try {
            String addressName = (String) hashMap.get("addressName");
            if (second != 0) {
                player.sendMessage(second + "秒后传送到:" + addressName);
                second--;
                return;
            }
            String worldName = (String) hashMap.get("worldName");

            List<World> worlds = player.getServer().getWorlds();
            World world = null;
            for (World worldT : worlds) {
                if (worldT.getName().equals(worldName)) {
                    world = worldT;
                }
            }
            player.teleport(new Location(world,
                    (Double) hashMap.get("x"),
                    (Double) hashMap.get("y"),
                    (Double) hashMap.get("z")
            ));
            player.sendMessage("已传送到: " + addressName);
            addressTpUserLocal.remove(player.getName());
            stop();
        } catch (Exception e) {
            e.printStackTrace();
            player.sendMessage("发生异常,请联系服务器负责人");
            stop();
        }
    }

    public void start() {
        this.runTaskTimer(Dict.PLUGIN, 0, 30L);
    }

    public void stop() {
        this.cancel();
    }
}
