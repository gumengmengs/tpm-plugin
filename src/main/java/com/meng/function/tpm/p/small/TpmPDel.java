package com.meng.function.tpm.p.small;

import com.google.gson.JsonObject;
import com.meng.function.http.TpmPHttp;
import com.meng.function.tpm.p.util.Local;
import com.meng.function.tpm.util.TpmCommon;
import com.meng.util.Dict;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpmPDel{
    public boolean onCommand(CommandSender sender, String arg) {
        Player player = TpmCommon.isOnline(sender,sender.getName());
        delAddress(player,arg);
        return true;
    }

    private void delAddress(Player player,String addressName){
        String str = new Local().delAddress(player.getName(), addressName);
        player.sendMessage(str);
    }

    private void serverDel(Player player,String addressName){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userName",player.getName());
        jsonObject.addProperty("addressName", addressName);
        new TpmPHttp(jsonObject, Dict.IP,"address/del",player,102).start();
    }
}
