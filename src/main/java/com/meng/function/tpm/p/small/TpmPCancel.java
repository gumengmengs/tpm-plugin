package com.meng.function.tpm.p.small;

import com.meng.function.http.TpmPHttp;
import com.meng.function.tpm.p.util.TpmPRunnable;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

import static com.meng.function.tpm.p.TpmP.addressTpUser;
import static com.meng.function.tpm.p.TpmP.addressTpUserLocal;

public class TpmPCancel {

    public boolean onCommand(@NotNull CommandSender sender) {
        TpmPHttp addressTp = addressTpUser.get(sender.getName());
        TpmPRunnable tpmPRunnable = addressTpUserLocal.get(sender.getName());
        if (addressTp != null) {
            addressTp.stop();
            addressTpUser.remove(sender.getName());
            sender.sendMessage("已取消地址传送");
        } else if (tpmPRunnable != null) {
            tpmPRunnable.stop();
            addressTpUserLocal.remove(sender.getName());
            sender.sendMessage("已取消地址传送");
        } else {
            sender.sendMessage("当前没有传送地址");
        }

        return true;
    }
}
