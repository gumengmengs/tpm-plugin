package com.meng.function.tpm.p.small;

import com.google.gson.JsonObject;
import com.meng.function.http.TpmPHttp;
import com.meng.function.tpm.p.util.Local;
import com.meng.function.tpm.util.TpmCommon;
import com.meng.util.Dict;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

public class TpmPGet{
    public boolean onCommand(@NotNull CommandSender sender) {
        getAllAddress(sender);
        return true;
    }

    private void getAllAddress(CommandSender sender){
        List<HashMap<String, Object>> allAddress = new Local().getAllAddress(sender.getName());

        if (allAddress.size() == 0 && Dict.ADDRESS_SERVER){
            Player player = TpmCommon.isOnline(sender,sender.getName());
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("userName",player.getName());
            new TpmPHttp(jsonObject, Dict.IP,"address/getAll",player,103).start();
            return;
        }
        if (allAddress.size() == 0){
            sender.sendMessage("未找到，请先添加一个地址试试吧");
        }
        for (HashMap<String, Object> address : allAddress) {
            sender.sendMessage(
                    "在<" + address.get("worldName") + ">世界里的地址: " + address.get("addressName")
            );
        }

    }
}
