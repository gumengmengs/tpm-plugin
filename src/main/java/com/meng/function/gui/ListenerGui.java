package com.meng.function.gui;

import com.meng.file.Address;
import com.meng.function.http.TpmPHttp;
import com.meng.function.tpm.p.small.TpmPCancelShare;
import com.meng.function.tpm.p.small.TpmPShare;
import com.meng.function.tpm.p.small.TpmPTp;
import com.meng.function.tpm.p.util.Local;
import com.meng.function.tpm.p.util.TpmPRunnable;
import com.meng.function.tpm.small.TpmCancel;
import com.meng.function.tpm.small.TpmTp;
import com.meng.function.tpm.util.TpmRunnable;
import com.meng.util.Dict;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.InventoryView;

import java.util.*;

import static com.meng.function.tpm.p.TpmP.addressTpUser;
import static com.meng.function.tpm.p.TpmP.addressTpUserLocal;
import static com.meng.function.tpm.util.TpmCommon.playerMap;

public class ListenerGui implements Listener {

    private static HashMap<String, String> shareAddress = new HashMap<>();

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        // 只有玩家可以触发 InventoryClickEvent，可以强制转换
        InventoryView inv = player.getOpenInventory();
        if (inv.title().equals(Component.text("菜单主页"))) {
            // 通过标题区分 GUI
            e.setCancelled(true);
            if (e.getCurrentItem() == null) return;
            if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(Dict.PLAYER_TITLE))) {
                new TpmGUI().playerTpm(player);
            } else if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(Dict.ADDRESS_TITLE))) {
                new TpmGUI().addressSelect(player);
            } else if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(Dict.CANCEL))) {
                TpmRunnable tpmThread = playerMap.get(player.getName());
                TpmPHttp addressTp = addressTpUser.get(player.getName());
                TpmPRunnable tpmPRunnable = addressTpUserLocal.get(player.getName());
                if (addressTp != null) {
                    addressTp.stop();
                    addressTpUser.remove(player.getName());
                    player.sendMessage("已取消地址传送");
                } else if (tpmPRunnable != null) {
                    tpmPRunnable.stop();
                    addressTpUserLocal.remove(player.getName());
                    player.sendMessage("已取消地址传送");
                } else if (tpmThread != null) {
                    tpmThread.cancel(player.getName());
                    player.sendMessage("已取消传送");
                } else {
                    new TpmCancel().onCommand(player);
                }
                player.closeInventory();
            }
        } else if (inv.title().equals(Component.text(Dict.PLAYER_TITLE))) {
            e.setCancelled(true);
            List<Player> playerList = new LinkedList<>(Bukkit.getOnlinePlayers());
            for (Player player1 : playerList) {
                if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text("传送到 " + player1.getName()))) {
                    new TpmTp().onCommand(player, player1.getName());
                    player.closeInventory();
                    break;
                }
            }
        } else if (inv.title().equals(Component.text(Dict.ADDRESS_TITLE))) {
            e.setCancelled(true);
            if (e.getCurrentItem() == null) return;
            if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(Dict.ADDRESS_TP))) {
                new TpmGUI().addressTp(player);
            } else if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(Dict.ADDRESS_SHARE))) {
                new TpmGUI().addressShare(player);
            } else if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(Dict.ADDRESS_SHARE_ALL))) {
                new TpmGUI().addressAllShare(player);
            }
        } else if (inv.title().equals(Component.text(Dict.ADDRESS_TP))) {
            e.setCancelled(true);
            List<HashMap<String, Object>> allAddress = new Local().getAllAddress(player.getName());
            for (Map<String, Object> map : allAddress) {
                if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text((String) map.get("addressName")))) {
                    new TpmPTp().onCommand(player, (String) map.get("addressName"));
                    player.closeInventory();
                }
            }
        } else if (inv.title().equals(Component.text(Dict.ADDRESS_SHARE))) {
            e.setCancelled(true);
            List<HashMap<String, Object>> allAddress = new Local().getAllAddress(player.getName());
            for (HashMap<String, Object> map : allAddress) {
                String address = (String) map.get("addressName");
                if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(address))) {
                    boolean b = new TpmPCancelShare().onCommand(player, address);
                    if (!b) {
                        shareAddress.put(player.getName(), address);
                        new TpmGUI().addressSharePlayer(player);
                    } else {
                        player.closeInventory();
                    }
                    return;
                }
            }

        } else if (inv.title().equals(Component.text(Dict.ADDRESS_SHARE_ALL))) {
            e.setCancelled(true);
            List<String> addressStr = new Address().getAddress().getStringList("allUser");
            for (String name : addressStr) {
                List<HashMap<String, Object>> allAddress = new Local().getAllAddress(name);
                for (HashMap<String, Object> map : allAddress) {
                    String address = (String) map.get("addressName");
                    if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(address))) {
                        if (Objects.requireNonNull(e.getCurrentItem().getItemMeta().lore()).size() != 0 &&
                                Objects.requireNonNull(e.getCurrentItem().getItemMeta().lore()).get(0).equals(Component.text("由" + name + "分享"))) {
                            new TpmPTp().tpmPAddress(player, address, name);
                            return;
                        }
                    }
                }
            }
        } else if (inv.title().equals(Component.text("分享其他玩家"))) {
            e.setCancelled(true);
            List<Player> playerList = new LinkedList<>(Bukkit.getOnlinePlayers());
            if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text("分享给所有玩家"))) {
                new TpmPShare().onCommand(player, shareAddress.get(player.getName()), null);
                player.closeInventory();
                return;
            }
            for (Player player1 : playerList) {
                if (Objects.equals(Objects.requireNonNull(e.getCurrentItem()).getItemMeta().displayName(), Component.text(player1.getName()))) {
                    new TpmPShare().onCommand(player, shareAddress.get(player.getName()), player1);
                    player.closeInventory();
                    break;
                }
            }
        }
    }
}
