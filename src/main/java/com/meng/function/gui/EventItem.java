package com.meng.function.gui;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class EventItem implements Listener {
    @EventHandler
    public void playerHandItem(PlayerInteractEvent e){
        ItemStack compass = new ItemStack(Material.COMPASS);
        if(e.getItem() != null){
            if (compass.getType().equals(e.getItem().getType())) {
                new TpmGUI().caiDan(e.getPlayer());
            }
        }
    }
}
