package com.meng.function.gui;

import com.meng.function.tpm.util.TpmCommon;
import com.meng.util.Dict;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class  MapDeleteRunnable extends BukkitRunnable {

    public int state = 0;

    @Override
    public void run() {
        for (Integer integer : TpmGUI.allPage.keySet()) {
            TpmGUI.allPage.remove(integer);
        }
        TpmGUI.mapDeleteRunnableList.remove(0);
        this.stop();
    }

    //三分钟执行一次
    public void start(){
        state = 1;
        this.runTaskTimer(Dict.PLUGIN,20L * 60 * 3,0);
    }

    public void stop(){
        this.cancel();
        Dict.PLUGIN.getLogger().info("map已清空");
        state = 0;
    }
}
