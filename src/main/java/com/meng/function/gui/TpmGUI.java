package com.meng.function.gui;

import com.meng.file.Address;
import com.meng.function.tpm.p.util.Local;
import com.meng.util.Dict;
import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class TpmGUI {

    public static HashMap<Integer, List<HashMap<String, Object>>> allPage = new HashMap<>();

    public static List<MapDeleteRunnable> mapDeleteRunnableList = new LinkedList<>();

    public void caiDan(Player user) {
        Inventory inv = Bukkit.createInventory(user, 9, Component.text("菜单主页"));

        ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta skullMeta = (SkullMeta) playerHead.getItemMeta();
        skullMeta.displayName(Component.text(Dict.PLAYER_TITLE));
        playerHead.setItemMeta(skullMeta);
        inv.setItem(0, playerHead);

        ItemStack grassBlock = new ItemStack(Material.GRASS_BLOCK);
        ItemMeta itemMeta = grassBlock.getItemMeta();
        itemMeta.displayName(Component.text(Dict.ADDRESS_TITLE));
        grassBlock.setItemMeta(itemMeta);
        inv.setItem(4, grassBlock);

        ItemStack minecart = new ItemStack(Material.MINECART);
        ItemMeta minecartMeta = minecart.getItemMeta();
        minecartMeta.displayName(Component.text(Dict.CANCEL));
        minecart.setItemMeta(minecartMeta);
        inv.setItem(8, minecart);
        user.openInventory(inv);
    }

    public void playerTpm(Player user) {
        List<Player> playerList = new LinkedList<>(Bukkit.getOnlinePlayers());
        Inventory inv = Bukkit.createInventory(user, 3 * 9, Component.text(Dict.PLAYER_TITLE));
        int i = 0;
        for (Player player : playerList) {
            String playerName = player.getName();
            ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD);
            SkullMeta skullMeta = (SkullMeta) playerHead.getItemMeta();
            skullMeta.setOwningPlayer(player);
            skullMeta.displayName(Component.text("传送到 " + playerName));
            playerHead.setItemMeta(skullMeta);
            inv.setItem(i++, playerHead);
        }
        user.openInventory(inv);
    }

    public void addressSelect(Player player) {
        Bukkit.getLogger().info(player.getName() + "打开了地址菜单选择");
        Inventory inv = Bukkit.createInventory(player, 9, Component.text(Dict.ADDRESS_TITLE));

        ItemStack grassBlock = new ItemStack(Material.GRASS_BLOCK);
        ItemMeta itemMeta = grassBlock.getItemMeta();
        itemMeta.displayName(Component.text(Dict.ADDRESS_TP));
        grassBlock.setItemMeta(itemMeta);
        inv.setItem(3, grassBlock);

        ItemStack hopperMinecart = new ItemStack(Material.HOPPER_MINECART);
        ItemMeta MetaHopper = hopperMinecart.getItemMeta();
        MetaHopper.displayName(Component.text(Dict.ADDRESS_SHARE));
        hopperMinecart.setItemMeta(MetaHopper);
        inv.setItem(4, hopperMinecart);

        ItemStack chestMinecart = new ItemStack(Material.CHEST_MINECART);
        ItemMeta MetaChest = chestMinecart.getItemMeta();
        MetaChest.displayName(Component.text(Dict.ADDRESS_SHARE_ALL));
        chestMinecart.setItemMeta(MetaChest);
        inv.setItem(5, chestMinecart);

        player.openInventory(inv);
    }

    public void addressTp(Player player) {
        Inventory inv = Bukkit.createInventory(player, 3 * 9, Component.text(Dict.ADDRESS_TP));
        List<HashMap<String, Object>> allAddress = new Local().getAllAddress(player.getName());
        for (HashMap<String, Object> map : allAddress) {
            ItemStack grassBlock = alike(map, (String) map.get("worldName"));
            inv.addItem(grassBlock);
        }
        player.openInventory(inv);
    }

    public void addressShare(Player player) {
        Inventory inv = Bukkit.createInventory(player, 3 * 9, Component.text(Dict.ADDRESS_SHARE));

        List<HashMap<String, Object>> allAddress = new Local().getAllAddress(player.getName());
        for (HashMap<String, Object> map : allAddress) {
            String isShare = (String) map.get("isShare");
            ItemStack grassBlock = alike(map, "分享状态: " + isShare);
            if (!isShare.equals("refuse")) {
                ItemMeta itemMeta = grassBlock.getItemMeta();
                itemMeta.addEnchant(Enchantment.DAMAGE_ALL, 100, true);
                grassBlock.setItemMeta(itemMeta);
            }
            inv.addItem(grassBlock);
        }
        player.openInventory(inv);
    }

    public void addressSharePlayer(Player player){
        Inventory inv = Bukkit.createInventory(player, 3 * 9, Component.text("分享其他玩家"));
        List<Player> playerList = new LinkedList<>(Bukkit.getOnlinePlayers());
        for (Player player1 : playerList) {
            String playerName = player1.getName();
            ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD);
            SkullMeta skullMeta = (SkullMeta) playerHead.getItemMeta();
            skullMeta.setOwningPlayer(player1);
            skullMeta.displayName(Component.text(playerName));
            playerHead.setItemMeta(skullMeta);
            inv.addItem(playerHead);
        }
        ItemStack stack = new ItemStack(Material.BEACON);
        ItemMeta itemMeta = stack.getItemMeta();
        itemMeta.displayName(Component.text("分享给所有玩家"));
        stack.setItemMeta(itemMeta);
        inv.addItem(stack);
        player.openInventory(inv);
    }

    public void addressAllShare(Player player) {
        int page = 1, i = 0;
        Inventory inv = Bukkit.createInventory(player, 3 * 9, Component.text(Dict.ADDRESS_SHARE_ALL));
        List<String> addressStr = new Address().getAddress().getStringList("allUser");
        List<HashMap<String, Object>> temp = new LinkedList<>();
        if (allPage.size() == 0 | allPage.get(page) == null) {
            if (mapDeleteRunnableList.size() < 1 || mapDeleteRunnableList.get(0) == null) {
                mapDeleteRunnableList.add(0, new MapDeleteRunnable());
                mapDeleteRunnableList.get(0).start();
                for (String s : addressStr) {
                    List<HashMap<String, Object>> allAddress = new Local().getAllAddress(s);
                    for (HashMap<String, Object> map : allAddress) {
                        if (i == 23) {
                            page += 1;
                            i = 0;
                        }
                        String isShare = (String) map.get("isShare");
                        if (isShare.equals("refuse")) continue;
                        map.put("shareUser", s);
                        temp.add(map);
                        allPage.put(page, temp);
                        i++;
                    }
                }
            }
        }
        int counter = 0;
        List<HashMap<String, Object>> hashMaps = allPage.get(page);
        for (HashMap<String, Object> hashMap : hashMaps) {
            ItemStack grassBlock = alike(hashMap, "由" + hashMap.get("shareUser") + "分享");
            if (counter == 18 || counter == 22) counter++;
            if (counter == 26) break;
            inv.setItem(counter++, grassBlock);
        }
        ItemStack grassBlock = new ItemStack(Material.ARROW);
        ItemMeta itemMeta = grassBlock.getItemMeta();
        itemMeta.displayName(Component.text("上一页"));
        grassBlock.setItemMeta(itemMeta);
        inv.setItem(18, grassBlock);

        ItemStack tripwireHook = new ItemStack(Material.TRIPWIRE_HOOK);
        ItemMeta tripwireHookMeta = tripwireHook.getItemMeta();
        tripwireHookMeta.displayName(Component.text("第 " + page + " 页"));
        tripwireHook.setItemMeta(tripwireHookMeta);
        tripwireHook.setAmount(page);
        inv.setItem(22, tripwireHook);

        ItemStack hopperMinecart;
        if (page > 1) {
            hopperMinecart = new ItemStack(Material.SPECTRAL_ARROW);
        } else {
            hopperMinecart = new ItemStack(Material.ARROW);
        }
        ItemMeta MetaHopper = hopperMinecart.getItemMeta();
        MetaHopper.displayName(Component.text("下一页"));
        hopperMinecart.setItemMeta(MetaHopper);
        inv.setItem(26, hopperMinecart);

        player.openInventory(inv);
    }


    private ItemStack alike(HashMap<String, Object> map, String text) {
        String addressName = (String) map.get("addressName");
        String worldName = (String) map.get("worldName");
        Double y = (Double) map.get("y");
        ItemStack grassBlock;
        if (worldName.equals("world_nether")) {
            grassBlock = new ItemStack(Material.NETHERRACK);
        } else if (worldName.equals("world_the_end")) {
            grassBlock = new ItemStack(Material.END_STONE);
        } else {
            if (y <= 0 && (Bukkit.getMinecraftVersion().equals("1.19.2")
                    || Bukkit.getMinecraftVersion().equals("1.19.1")
                    || Bukkit.getMinecraftVersion().equals("1.19.0"))) {
                grassBlock = new ItemStack(Material.DEEPSLATE);
            } else if (y <= 55) {
                grassBlock = new ItemStack(Material.STONE);
            } else {
                grassBlock = new ItemStack(Material.GRASS_BLOCK);
            }
        }
        ItemMeta address = grassBlock.getItemMeta();
        address.displayName(Component.text(addressName));
        List<Component> list = new LinkedList<>();
        list.add(Component.text(text));
        address.lore(list);
        grassBlock.setItemMeta(address);
        return grassBlock;
    }
}
