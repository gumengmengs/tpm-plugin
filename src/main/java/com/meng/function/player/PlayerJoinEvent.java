package com.meng.function.player;

import com.meng.file.Address;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.List;

public class PlayerJoinEvent implements Listener {
    @EventHandler(priority= EventPriority.HIGHEST)
    public void playerJoin(org.bukkit.event.player.PlayerJoinEvent e){
        Player player = e.getPlayer();
        Address address = new Address();
        List<String> allUser = address.getAddress().getStringList("allUser");
        if (allUser.size() == 0) {
            allUser.add(player.getName());
            address.getAddress().set("allUser",allUser);
            address.saveAddress();
        }else{
            int i = 0;
            for (String name : allUser) {
                if (name.equals(player.getName())){
                    i++;
                }
            }
            if (i == 0){
                allUser.add(player.getName());
                address.getAddress().set("allUser",allUser);
                address.saveAddress();
            }
        }
    }
}
