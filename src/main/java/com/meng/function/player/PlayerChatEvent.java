package com.meng.function.player;

import com.google.gson.JsonObject;
import com.meng.function.http.HttpDispose;
import com.meng.util.Dict;
import io.papermc.paper.event.player.AbstractChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.net.InetAddress;

public class PlayerChatEvent implements Listener{
    @EventHandler(priority= EventPriority.HIGHEST)
    public void AsyncPlayerChatListener(AbstractChatEvent e) {
        InetAddress address = e.getPlayer().getAddress().getAddress();
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("userName",e.getPlayer().getName());
        jsonObject.addProperty("msg",e.message().toString());
        jsonObject.addProperty("ip",address.getHostAddress());
        new HttpDispose(jsonObject,Dict.IP,"chatRecord").start();
        Bukkit.getLogger().info(e.getPlayer().getName() + ":" + e.message());
    }
}
