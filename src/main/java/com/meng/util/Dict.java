package com.meng.util;

import com.meng.Main;

public class Dict {
    public static final Main PLUGIN = Main.getPlugin(Main.class);
    public static final String IP = PLUGIN.getConfig().getString("service.ip");

    public static final Integer TPM_P_SECOND = PLUGIN.getConfig().getInt("tpm-p.second");

    public static final Integer TPM_SECOND = PLUGIN.getConfig().getInt("tpm.second");

    public static final boolean ADDRESS_SERVER = PLUGIN.getConfig().getBoolean("data.address.server");

    public static final boolean SERVICE = PLUGIN.getConfig().getBoolean("service.enable");

    public static final String PLAYER_TITLE = "玩家传送";
    public static final String ADDRESS_TITLE = "地址功能选择";
    public static final String ADDRESS_TP = "地址传送";
    public static final String ADDRESS_SHARE = "地址分享";
    public static final String ADDRESS_SHARE_ALL = "全部分享";
    public static final String CANCEL = "取消当前操作";

}
