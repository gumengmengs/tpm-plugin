package com.meng;

import com.meng.file.Address;
import com.meng.function.gui.EventItem;
import com.meng.function.gui.ListenerGui;
import com.meng.function.gui.TpmCd;
import com.meng.function.player.PlayerChatEvent;
import com.meng.function.player.PlayerJoinEvent;
import com.meng.function.tpm.Tpm;
import com.meng.function.tpm.p.TpmP;
import com.meng.util.Dict;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();
        if (Dict.SERVICE){
            if (this.getConfig().getBoolean("data.chat.server")){
                this.getServer().getPluginManager().registerEvents(new PlayerChatEvent(), this);
            }
        }
        init();
        say("插件" + this.getDescription().getName() + "版本为:" + this.getDescription().getVersion());
    }

    @Override
    public void onDisable() {
        say("插件" + this.getDescription().getName() + "已成功退出");
    }

    public void say(String s) {
        CommandSender sender = Bukkit.getConsoleSender();
        sender.sendMessage(s);
    }

    private void init() {
        Bukkit.getPluginCommand("tpm").setExecutor(new Tpm());
        Bukkit.getPluginCommand("tpm-p").setExecutor(new TpmP());
        Bukkit.getPluginCommand("tpm-cd").setExecutor(new TpmCd());
        Bukkit.getPluginManager().registerEvents(new ListenerGui(),this);
        Bukkit.getPluginManager().registerEvents(new EventItem(),this);
        Bukkit.getPluginManager().registerEvents(new PlayerJoinEvent(),this);
        new Address().saveDefaultAddress();
    }

}
